'use strict';

/**
 * crossTab
 * Vanilla JS
 */

/**
 * Doc is the main class to manage data
 */
class Doc {
  changeEvt = new Event('change');
  evt = new EventTarget();
  name = '';
  data = '';

  setDoc(name, data) {
    this.name = name;
    this.data = data;
    this.evt.dispatchEvent(this.changeEvt);
  }

  setName(name) {
    this.name = name;
    this.evt.dispatchEvent(this.changeEvt);
  }

  setData(data) {
    this.data = data;
    this.evt.dispatchEvent(this.changeEvt);
  }
}
const doc = new Doc();

const pref = {
  separator: "",
  quote: "'",
  newline: "\n"
}

/**
 * Listener
 */
document.getElementById('inputFile').addEventListener('change', handleFile);
doc.evt.addEventListener('change', changeEvent);

function changeEvent() {
  if (doc.name.endsWith("tsv")) {
    pref.separator = "\t";
  } else {
    pref.separator = ",";
  }
  showFileName(doc.name);
  showData(doc.data);
}

/**
 * Functions / public function
 */

/**
 * init the document once time when the body is load
 */
window.onload = function (){
  const name = localStorage.getItem('name');
  const data = localStorage.getItem('data');
  if (name && data) {
    doc.setDoc(name, data);
    switchHiddenNameAndSelectFile();
  }
}

/**
 * Close the file, delete local storage
 */
function closeFile() {
  localStorage.removeItem('name');
  localStorage.removeItem('data');
  doc.setDoc('', '');
  switchHiddenNameAndSelectFile();
}

/*
* Utilities / private function
*/

/**
 * Catch the inputFile change event and shows up the file
 */
function handleFile() {
  let selectedFile = this.files[0]; /* now you can work with the file list */
  doc.setName(selectedFile.name);
  readAndShowFile(selectedFile);
  switchHiddenNameAndSelectFile();
}

/**
 * Displays the file’s name and set the link to download the file
 * @param name : string
 *
 */
function showFileName(name) {
  const a = document.getElementById('fileName');
  a.innerText = name;
  a.setAttribute("download", name);
  const downloadableFile = new File([doc.data], doc.name, {type: 'text/plain'});
  const url = URL.createObjectURL(downloadableFile);
  a.setAttribute("href", url)
}

/**
 * Reads the file and shows it
 * @param selectedFile : File
 */
function readAndShowFile(selectedFile) {
  let reader = new FileReader();
  reader.onload = function (event) {
    if (typeof event.target.result === 'string') {
      showData(event.target.result);
      keepDataInBrowser(selectedFile.name, event.target.result);
      doc.setData(event.target.result);
    }
  }
  reader.readAsText(selectedFile);
}

/**
 * Takes file's data and returns a li HTML list
 * @param data : string
 * @return string
 */
function fileDataToList(data) {
  // atoms:  string[molecule][atom]
  let atoms = data.split(pref.newline).map(l => l.split(pref.separator));
  const headers = atoms.shift();
  const molecules = atoms
    .filter(molecule => !(molecule.length === 1 && molecule[0].length === 0))
    .map(molecule => addHeader(headers, molecule))
    .map(m => '<ul>' + m + '</ul>');

  if (molecules.length !== 0) {
    const list = molecules
      .map(molecule => '<li>' + molecule + '</li>')
      .reduce((p, c) => p + c);
    return '<div><ol>' + list + '</ol></div>';
  } else {
    return "";
  }
}

/**
 *
 * @param headers string[]
 * @param line string[]
 * @return string
 */
function addHeader(headers, line) {
  let result = [];
  for (let i = 0; i < line.length; i++) {
    result.push(`<li>${headers[i]}: ${line[i]}</li>`)
  }
  return result.reduce((p, c) => p + c);
}

/**
 * Takes file's data and returns an HTML table
 * @param data : string
 * @return string
 */
function fileDataToTable(data) {
  const lines = data
    .split(pref.newline)
    .filter(line => line.length > 1)
    .map(line => '<tr><td tabindex="0">'.concat(line.split(pref.separator).join('</td><td tabindex="0">').concat('</td></tr>')))
    .join('');
  return '<table><tbody>' + lines + '</tbody></table>';
}

/**
 * Keep the selected file in the browser's local storage
 * @param fileName: string
 * @param fileData: string
 */
function keepDataInBrowser(fileName, fileData) {
  localStorage.name = fileName;
  localStorage.data = fileData;
}

/**
 * Shows data in table and list
 * @param data : string
 */
function showData(data) {
  document.getElementById('fileOutput').innerHTML = data ? fileDataToTable(data) : '';
  document.getElementById('listOutput').innerHTML = data ? fileDataToList(data) : '';
}

function switchHiddenNameAndSelectFile() {
  document.getElementById('fileNameDiv').hidden = !document.getElementById('fileNameDiv').hidden;
  document.getElementById('inputFileDiv').hidden = !document.getElementById('inputFileDiv').hidden;
}

function reloadFile() {
  console.info(doc.data);
  showData(doc.data);
}
